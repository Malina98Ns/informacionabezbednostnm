package xml;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class CreateXMLDOM {
	public static void createXML(String email, String eSubject, String eBody) {
		final String xmlFilePath = "./data/" + email + ".xml";

		DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();
		
		try {
			
			
			
			DocumentBuilder documentBuilder = documentFactory.newDocumentBuilder();
			
			Document document = documentBuilder.newDocument();
			
			// root element
			Element root = document.createElement("email");
			document.appendChild(root);
			
			//subject element
			root.appendChild(getEmailElements(document, "subject", eSubject));
			
			//body element
			root.appendChild(getEmailElements(document, "body", eBody));
			
			//create the xml file
			//transfrom the DOM object to an XML File
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource domSource = new DOMSource(document);
			StreamResult streamResult = new StreamResult(new File(xmlFilePath));
			
			// If you use
            // StreamResult result = new StreamResult(System.out);
            // the output will be pushed to the standard output ...
            // You can use that for debugging 
 
			transformer.transform(domSource, streamResult);
			
			printEmail(document);
			System.out.println("Done creating XML File");
			
		} catch(ParserConfigurationException pce) {
			pce.printStackTrace();
		} catch(TransformerException tfe) {
			tfe.printStackTrace();
		}
	}

	private static Node getEmailElements(Document doc, String name, String value) {
		Element node = doc.createElement(name);
		node.appendChild(doc.createTextNode(value));
		return node;
	}

	

	// Ispisivanje poruke iz dokumenta
	public static void printEmail(Document doc) {
		Node fc = doc.getFirstChild();
		NodeList list = fc.getChildNodes();
		for (int i = 0; i < list.getLength(); i++) {
			Node node = list.item(i);
			if ("subject".equals(node.getNodeName())) {
				System.out.println("Subject: " + node.getTextContent());
			}
			if ("body".equals(node.getNodeName())) {
				System.out.println("Body: " + node.getTextContent());
			}
		}
	}
	
	// Ispisivanje u konzoli
		public static void printDocument(Document doc, OutputStream out) throws IOException, TransformerException {
			TransformerFactory tf = TransformerFactory.newInstance();
			Transformer transformer = tf.newTransformer();
			transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
			transformer.setOutputProperty(OutputKeys.METHOD, "xml");
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
			transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");

			transformer.transform(new DOMSource(doc), new StreamResult(new OutputStreamWriter(out, "UTF-8")));
		}
}
