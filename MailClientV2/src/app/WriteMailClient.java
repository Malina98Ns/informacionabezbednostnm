package app;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.security.PublicKey;
import java.security.Security;
import java.security.cert.Certificate;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.mail.internet.MimeMessage;

import org.apache.xml.security.utils.JavaUtils;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

import com.google.api.services.gmail.Gmail;

import model.mailclient.MailBody;
import signature.SignEnveloped;
import util.Base64;
import util.GzipUtil;
import util.IVHelper;
import util.KeyStoreReader;
import xml.AsymmetricKeyDecryption;
import xml.AsymmetricKeyEncryption;
import xml.CreateXMLDOM;
import support.MailHelper;
import support.MailWritter;

public class WriteMailClient extends MailClient {

	
	public static void main(String[] args) {
		
        try {
        	Gmail service = getGmailService();
            
        	
        	System.out.println("Insert your email address:");
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            String sender = reader.readLine();
            final String xmlFilePath = "./data/" + sender + "_enc.xml";
            
            
        	
            System.out.println("Insert a receiver:");
            String receiver = reader.readLine();
            
            System.out.println("Insert a subject:");
            String subject = reader.readLine();
            
            
            System.out.println("Insert body:");
            String body = reader.readLine();
            
            CreateXMLDOM.createXML(sender, subject, body);
            
            SignEnveloped.testIt(sender);
            
            AsymmetricKeyEncryption.testIt(sender, receiver);
            
            
            /*Compression
            String compressedSubject = Base64.encodeToString(GzipUtil.compress(subject));
            String compressedBody = Base64.encodeToString(GzipUtil.compress(body));
            
            //Key generation
            KeyGenerator keyGen = KeyGenerator.getInstance("AES"); 
			SecretKey secretKey = keyGen.generateKey();
			Cipher aesCipherEnc = Cipher.getInstance("AES/CBC/PKCS5Padding");
			
			//inicijalizacija za sifrovanje 
			IvParameterSpec ivParameterSpec1 = IVHelper.createIV();
			aesCipherEnc.init(Cipher.ENCRYPT_MODE, secretKey, ivParameterSpec1);
			
			
			//sifrovanje
			byte[] ciphertext = aesCipherEnc.doFinal(compressedBody.getBytes());
			String ciphertextStr = Base64.encodeToString(ciphertext);
			System.out.println("Kriptovan tekst: " + ciphertextStr);
			
			
			//inicijalizacija za sifrovanje 
			IvParameterSpec ivParameterSpec2 = IVHelper.createIV();
			aesCipherEnc.init(Cipher.ENCRYPT_MODE, secretKey, ivParameterSpec2);
			
			byte[] ciphersubject = aesCipherEnc.doFinal(compressedSubject.getBytes());
			String ciphersubjectStr = Base64.encodeToString(ciphersubject);
			System.out.println("Kriptovan subject: " + ciphersubjectStr);
			
			KeyStoreReader ksr = new KeyStoreReader();
			
			ksr.load(new FileInputStream("./data/usera.jks"), "123");
			Certificate certUserB = ksr.getCertificate("tomel");
			PublicKey pk = certUserB.getPublicKey();
			Cipher rsaCipherEnc = Cipher.getInstance("RSA/ECB/PKCS1Padding");
			rsaCipherEnc.init(Cipher.ENCRYPT_MODE, pk);
			byte[] sifrovanKljuc = rsaCipherEnc.doFinal(secretKey.getEncoded());
			System.out.println("Kriptovan kljuc: " + Base64.encodeToString(sifrovanKljuc));
			
			MailBody mb = new MailBody(ciphertextStr, ivParameterSpec1.getIV(), ivParameterSpec2.getIV(), sifrovanKljuc);
			
			//snimaju se bajtovi kljuca i IV.
			JavaUtils.writeBytesToFilename(KEY_FILE, secretKey.getEncoded());
			JavaUtils.writeBytesToFilename(IV1_FILE, ivParameterSpec1.getIV());
			JavaUtils.writeBytesToFilename(IV2_FILE, ivParameterSpec2.getIV());*/

			//Sending mail
        	MimeMessage mimeMessage = MailHelper.createMimeMessage(receiver, xmlFilePath);
        	MailWritter.sendMessage(service, "me", mimeMessage);
        	
        }catch (Exception e) {
        	e.printStackTrace();
		}
	}
	
	static {
		
		Security.addProvider(new BouncyCastleProvider());
		org.apache.xml.security.Init.init();
	}
}



